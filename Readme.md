# Readme:

### Description:
This projects scrapes a site or pdf and extracts the menu for the week. 

This is then output to the console in json-format or saved to a file.
It is also possible to get it to output to a S3-bucket, but should only be done in production.

The generated json-file is used by the frontend to display the menu.

### Requirements: 
- Nodejs
- npm
### Installation:

1: Install packages:  
 run `npm install`

2: Run reader/writer:   
`npm run manual`

Check for output in either the output-folder or console.

Switch the reader or writer class in manual.ts 

Do NOT use the bucketwriter!!