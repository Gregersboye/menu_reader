import {expect} from 'chai';
import Menu from '../../src/models/menu';

describe('Menu', () => {
    describe('Filename', function () {
        it('should derive filename from type-property', function () {
            const menu = new Menu('test');
            expect(menu.fileName).to.equal('test_current.json');
        });
    });
    describe('Type', function () {
        it('should use default-value for type', function () {
            const menu = new Menu();
            expect(menu.type).to.equal('default');
        });
        it('should set type if supplied in constructor', function () {
            const menu = new Menu('type');
            expect(menu.type).to.equal('type');
        });
    });
    describe('weeknumber', function () {
        it('Should set the weeknumber if supplied', function () {
            const menu = new Menu('type', 32);
            expect(menu.weekNumber).to.equal(32);
        });

        it('Should use null if weeknumber is not supplied', function () {
            const menu = new Menu();
            expect(menu.weekNumber).to.be.null;
        })
    });
});
// AssertionError: expected 43 to equal 42.