import { afterEach, beforeEach } from 'mocha';
import sinon from 'sinon';
import axios from 'axios';
import MadMedMestWebReader from '../../../src/Readers/MadMedMest/MadMedMestWebReader';
import { expect } from 'chai';
import * as fs from 'node:fs';

describe('MadMedMest', () => {
    let stub;
    describe('Webreader', function () {
        beforeEach(function () {
            const source = fs.readFileSync('./test/readers/MadMedMest/source.html', 'utf8');
            stub = sinon.stub(axios, 'get').resolves(source);
        });
        afterEach(function(){
            stub.restore();
        })
        it('Uses the correct url', function (done) {
            const menuCrawler = new MadMedMestWebReader(axios);
            menuCrawler.readMenu();
            expect(stub.withArgs('https://madmed-mest.dk/ugens-menu/').calledOnce).to.be.true;
            done();
        });
    });
});