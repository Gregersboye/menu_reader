import Menu from '../models/menu';

export default interface ReaderInterface {
    readMenu(): Promise<Menu[]>;
};;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
