import WriterInterface from './interfaces/WriterInterface';
import ReaderInterface from './interfaces/ReaderInterface';
import Menu from './models/menu';

export default class AutoCrawler {
    private reader: ReaderInterface;
    private writer: WriterInterface;

    constructor(reader: ReaderInterface, writer: WriterInterface) {
        this.writer = writer;
        this.reader = reader;
    }
    async  scrape() {
        console.log('scraping');
        const menus = await this.reader.readMenu();

        const promiseList = menus.map((menu: Menu) => {
            return this.writer.write(menu);
        });

        await Promise.all(promiseList);

        return true;

    }
}

