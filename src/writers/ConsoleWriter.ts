import WriterInterface from '../interfaces/WriterInterface';
import Menu from '../models/menu';

export default class ConsoleWriter implements WriterInterface {
    write(menu: Menu): Promise<any> {
        console.log(menu);

        return Promise.resolve();
    }
}