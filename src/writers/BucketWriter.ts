import Menu from '../models/menu';
import WriterInterface from '../interfaces/WriterInterface';
import AWS from 'aws-sdk';

export default class BucketWriter implements WriterInterface {
    private readonly s3: AWS.S3;
    private readonly cloudFront: AWS.CloudFront;
    private readonly bucket: string;

    constructor(s3: AWS.S3, cloudFront: AWS.CloudFront, bucket: string) {
        this.s3 = s3;
        this.cloudFront = cloudFront;
        this.bucket = bucket;
    }

    async write(menu: Menu) {
        await this.writeToBucket(menu);
        await this.invalidateCloudfront(menu.fileName);
    }

    private async invalidateCloudfront(key: string) {
        const cloudFrontParams = {
            DistributionId: 'EXNABXD07RW72',
            InvalidationBatch: {
                CallerReference: new Date().valueOf().toString(),
                Paths: {
                    Quantity: 1,
                    Items: [`/${key}`]
                }
            }
        };
        console.log('Invalidating cloudfront', cloudFrontParams);

        await this.cloudFront.createInvalidation(cloudFrontParams).promise();
    }

    private async writeToBucket(menu: Menu) {
        const key = menu.fileName;
        console.log(`Uploading file ${key} to bucket ${this.bucket}: `, menu);
        const params = {
            Bucket: this.bucket,
            Key: key,
            Body: JSON.stringify(menu)
        };

        await this.s3.putObject(params).promise();

    }
}
