import WriterInterface from '../interfaces/WriterInterface';
import Menu from '../models/menu';

export default class DynamoWriter implements WriterInterface{
    write(menu: Menu): Promise<any> {
        throw new Error('method \'write\' has not been implemented in DynamoDB');

        return Promise.resolve(menu);
    }
}