import * as fs from 'fs';
import Path from 'path';
import Menu from '../models/menu';
import WriterInterface from '../interfaces/WriterInterface';

export default class FileWriter implements WriterInterface {
    async write(menu : Menu) {
        const jsonData = JSON.stringify(menu, null, 3);

        console.log('writing to file: ', menu.fileName);
        fs.writeFileSync(Path.join('.', 'output', menu.fileName), jsonData);
    }
}
