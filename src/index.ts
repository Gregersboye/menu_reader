import AutoCrawler from './autoCrawler';

import MadMedMestWebReader from './Readers/MadMedMest/MadMedMestWebReader';
import BucketWriter from './writers/BucketWriter';
import AWS from 'aws-sdk';
import axios from 'axios';

exports.handler = async () => {
    const s3 = new AWS.S3();
    const cloudfront = new AWS.CloudFront();
    const menuCrawler = new MadMedMestWebReader(axios);
    const bucketWriter = new BucketWriter(s3, cloudfront, 'kantinemenu.gregersboye.dk');
    const autoCrawler = new AutoCrawler(menuCrawler, bucketWriter);

    return await autoCrawler.scrape();

};
