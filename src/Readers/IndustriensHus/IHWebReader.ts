import Menu from '../../models/menu';
import ReaderInterface from '../../interfaces/ReaderInterface';
import pdf from 'pdf-parse';
import dayjs from 'dayjs';
import advanced from 'dayjs/plugin/advancedFormat';
import weekOfYear from 'dayjs/plugin/weekOfYear';
import axios from 'axios';

dayjs.extend(advanced);
dayjs.extend(weekOfYear);
const dayList: Array<string> = ['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'mandag', 'tirsdag', 'onsdag', 'torsdag', 'fredag'];
//https://www.danskindustri.dk/globalassets/dokumenter-analyser-publikationer-mv/pdfer/lejere/ugens-menu/2024/menu-uge-23-24.pdf?v=240605
export default class IHWebReader implements ReaderInterface {
    private getUrl(): string {
        const date = dayjs();
        const week = date.format('ww-YY');

        return `https://www.danskindustri.dk/globalassets/dokumenter-analyser-publikationer-mv/pdfer/lejere/ugens-menu/${date.year()}/menu-uge-${week}.pdf`;
    }

    async readMenu(): Promise<Menu[]> {
        const url = this.getUrl();
        const response = await axios(url, {responseType: 'arraybuffer'});
        const dataBuffer = Buffer.from(response.data,);

        return await this.readMenuFromBuffer(dataBuffer);
    }

    private async readMenuFromBuffer(pdfBuffer): Promise<Menu[]> {
        const data: any = await pdf(pdfBuffer);

        return this.fileReadCallback(data.text);
    }

    private fileReadCallback(data: string): Menu[] {
        const menu = new Menu();
        const weekRegex: RegExp = /\suge\s*(\d{1,2})/i;
        let dayCounter: number = -1;
        const week: RegExpExecArray | null = weekRegex.exec(data);

        if (week !== null) {
            menu.weekNumber = parseInt(week[1]);
            menu.type = 'ih';
        }
        
        let writeToList: boolean = false;
        const regex: RegExp = /^\s*\n/mg;

        data.replace(regex, '')
            .split('\n')
            .map(line => line.trim().replace(':', ''))
            .filter((line: string) => line !== '')

            .forEach((line) => {
                const trimLine: string = line.trim().replace(':', '');
                if (dayList.includes(trimLine.toLowerCase())) {
                    writeToList = true;
                }
                if (writeToList && !dayList.includes(trimLine.toLowerCase())) {
                    menu.days[dayCounter].push(trimLine);
                }
                if (dayList.includes(trimLine.toLowerCase())) {
                    dayCounter += 1;
                    menu.days[dayCounter] = [];
                }
            });

        return [menu];
    }
}
