import Menu from '../../models/menu';
import ReaderInterface from '../../interfaces/ReaderInterface';
import pdf from 'pdf-parse';
import * as fs from 'fs';
const dayList: Array<string> = ['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'mandag', 'tirsdag', 'onsdag', 'torsdag', 'fredag'];
const enders: Array<string> = ['netcompany'];

export default class MadMedMestFileReader implements ReaderInterface {
    private readonly menuPath: string;
    
    constructor(menuPath: string) {
        this.menuPath = menuPath;
    }
    async readMenu(): Promise<Menu[]> {
        const dataBuffer = fs.readFileSync(this.menuPath);

        return await this.readMenuFromBuffer(dataBuffer);
    }

    private async readMenuFromBuffer(pdfBuffer): Promise<Menu[]> {
        const data: any = await pdf(pdfBuffer);

        return this.fileReadCallback(data.text);
    }

    private fileReadCallback(data: string): Menu[] {
        const menu = new Menu();
        const weekRegex: RegExp = /\suge\s*(\d{1,2})/i;
        let dayCounter: number = -1;
        const week = weekRegex.exec(data);

        if(week !== null){
            menu.weekNumber = parseInt(week[1]);
            menu.type = data.toLowerCase().includes('specialmenu') ? 'specialmenu' : 'menu';
        }

        let writeToList : boolean = false;
        const regex: RegExp = /^\s*\n/mg;

        data.replace(regex, '')
            .split('\n')
            .forEach((line) => {
                const trimLine : string= line.trim().replace(':', '');
                if (dayList.includes(trimLine.toLowerCase())) {
                    writeToList = true;
                }
                if(trimLine === ''){

                    return;
                }

                if (writeToList && !dayList.includes(trimLine.toLowerCase()) && !enders.includes(trimLine.toLowerCase())) {
                    menu.days[dayCounter].push(trimLine);

                    return;
                }

                if (dayList.includes(trimLine.toLowerCase())) {
                    dayCounter += 1;
                }
            });

        return [menu];
    }
}
