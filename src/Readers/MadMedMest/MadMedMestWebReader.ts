import { parse } from 'node-html-parser';
import Menu from '../../models/menu';
import ReaderInterface from '../../interfaces/ReaderInterface';
import { AxiosResponse, AxiosStatic } from 'axios';
import Day from '../../models/day';

export default class MadMedMestWebReader implements ReaderInterface {
    private readonly url: string;
    private axios: AxiosStatic;

    constructor(axios: AxiosStatic) {
        this.axios = axios;

        this.url = 'https://madmed-mest.dk/ugens-menu/';
    }

    private async crawl() {
        return await this.axios.get(this.url);
    }

    async readMenu(): Promise<Menu[]> {
        const html: AxiosResponse<any> = await this.crawl();
        const source = parse(html.data);
        const weekNumber: number = this.getWeekNumber(source);
        const menuList: Menu[] = [new Menu('menu', weekNumber), new Menu('special_menu', weekNumber)];

        const daysSeen: string[] = [];
        const days = source.querySelectorAll('.ugeplan_content');
        days.forEach((dayCode) => {
            let menu: Menu = menuList[1];
            const dayName: string = dayCode.querySelector('.ugeplan_dag')?.text.trim() ?? 'dag';
            if (!daysSeen.includes(dayName)) {
                daysSeen.push(dayName);
                menu = menuList[0];
            }
            const courses = dayCode.querySelectorAll('.ugeplan_text > p');

            const dayList: string[] = courses.map((course) => course.text);
            const day: Day = new Day(dayName, dayList);
            menu.days.push(day);
        });

        return menuList;
    }

    private getWeekNumber(source): number {

        const weekInfo = source.querySelectorAll('.ct-headline.heading-medium-large');
        const weekRegex: RegExp = /uge\s*(\d{1,2})/i;

        return weekInfo.reduce((currentNumber: number, blop: { text: string; }): number => {
            const matches: RegExpMatchArray | null = blop.text.match(weekRegex);
            if (matches != null) {
                return parseInt(matches[1]);
            }

            return currentNumber;
        }, 0);

    }
}
