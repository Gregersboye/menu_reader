export default class Day {
    name: string;
    courses: Array<string>;

    constructor(day, courses) {
        this.name = day;
        this.courses = courses;
    }
}