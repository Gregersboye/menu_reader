import Day from './day';

export default class Menu {
    weekNumber: number | null;
    days : Array<Day> = [];
    type: string;

    constructor(type : string = 'default', weekNumber : number|null = null) {
        this.type = type;
        this.weekNumber = weekNumber;
    }

    get fileName(){
        return `${this.type}_current.json`;
    }
}
