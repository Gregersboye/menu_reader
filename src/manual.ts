import AutoCrawler from './autoCrawler';
import ConsoleWriter from './writers/ConsoleWriter';
import MadMedMestWebReader from './Readers/MadMedMest/MadMedMestWebReader';
import axios from 'axios';
import ReaderInterface from './interfaces/ReaderInterface';
import WriterInterface from './interfaces/WriterInterface';

async function scrapeSite() {

    const menuCrawler: ReaderInterface = new MadMedMestWebReader(axios);
    const writer:  WriterInterface= new ConsoleWriter();
    const autoCrawler = new AutoCrawler(menuCrawler, writer);

    return await autoCrawler.scrape();

}

scrapeSite();
// readFile();
